﻿using UnityEngine;

public class PocketsController : MonoBehaviour {
	public GameObject whiteBall;

	private Vector3 originalWhiteBallPosition;

	void Start() {
		originalWhiteBallPosition = whiteBall.transform.position;
	}

	void OnCollisionEnter(Collision collision) {
		
		if (whiteBall.transform.name == collision.gameObject.name) {
			whiteBall.transform.position = originalWhiteBallPosition;
			whiteBall.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			GameController.GameControllerInstance.WhiteBallPocketed ();
		} else {
			GameController.GameControllerInstance.AddTempBallPocketed();
			GameObject.Destroy(collision.gameObject);
		}
	}
}