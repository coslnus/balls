﻿using UnityEngine;
using System;

namespace GameStates {
	public class StrikeState : AbstractGameObjectState {
		private GameController gameController;
		private GameObject cue;
		private GameObject whiteBall;
		private Vector3 cueOriginalPosition;

		private float speed = 30f;
		private float force = 0f;

		public StrikeState(MonoBehaviour parent, Vector3 cueOriginalPosition) : base(parent) { 
			gameController = (GameController)parent;
			cue = gameController.cue;
			whiteBall = gameController.whiteBall;

			float forceAmplitude = gameController.maxForce - gameController.minForce;
			float relativeDistance = (Vector3.Distance(cue.transform.position, whiteBall.transform.position) - gameController.minDistance) 
				/ (gameController.maxDistance - gameController.minDistance);
			force = forceAmplitude * relativeDistance + gameController.minForce;

			this.cueOriginalPosition = cueOriginalPosition;
		}

		public override void FixedUpdate () {
			float distance = Vector3.Distance(cue.transform.position, whiteBall.transform.position);
			if (distance < gameController.minDistance) {
				cue.GetComponent<Renderer>().enabled = false;
				cue.transform.position = cueOriginalPosition;
				whiteBall.GetComponent<Rigidbody>().AddForce(gameController.strikeDirection * force);
				gameController.currentState = new GameStates.WaitingForNextTurnState(gameController);
			} else {
				cue.transform.Translate(Vector3.down * speed * (-1) * Time.fixedDeltaTime);
			}
		}
	}
}