﻿using UnityEngine;

namespace GameStates {
	public class StrikingState : AbstractGameObjectState {
		private GameController gameController;

		private GameObject cue;
		private Vector3 cueOriginalPosition;
		private GameObject whiteBall;

		private float cueDirection = -1;
		private float speed = 7;

		public StrikingState(MonoBehaviour parent) : base(parent) { 
			gameController = (GameController)parent;
			cue = gameController.cue;
			whiteBall = gameController.whiteBall;
			cueOriginalPosition = cue.transform.position;
		}

		public override void Update() {
			if (Input.GetButtonUp("Fire1")) {
				cueDirection = -1;
				gameController.currentState = new GameStates.StrikeState(gameController, cueOriginalPosition);
			}
		}

		public override void FixedUpdate () {
			float distance = Vector3.Distance(cue.transform.position, whiteBall.transform.position);

			if (distance < gameController.minDistance || distance > gameController.maxDistance) {
				cueDirection *= -1;	
			}

			cue.transform.Translate(Vector3.down * cueDirection * speed * Time.fixedDeltaTime);
		}
	}
}