﻿using UnityEngine;

namespace GameStates {
	public class WaitingForStrikeState : AbstractGameObjectState {
		private GameController gameController;

		private GameObject cue;
		private GameObject cuePivot;
		private GameObject whiteBall;
		private Vector3 lookTarget;

		public WaitingForStrikeState(MonoBehaviour parent) : base(parent) { 
			gameController = (GameController)parent;
			cue = gameController.cue;
			cuePivot = gameController.cuePivot;
			whiteBall = gameController.whiteBall;

			cue.GetComponent<Renderer>().enabled = true;
		}

		public override void Update() {
			if (gameController.currentPlayer.Points > Mathf.Floor(gameController.commonBallsAmount / 2) 
				|| gameController.otherPlayer.Points > Mathf.Floor(gameController.commonBallsAmount / 2)) {
				gameController.EndMatch();
				return;
			}

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				lookTarget = hit.point;
			}
			cuePivot.transform.LookAt(new Vector3(lookTarget.x, cuePivot.transform.position.y, lookTarget.z));

			gameController.strikeDirection = (whiteBall.transform.position 
				- new Vector3(cue.transform.position.x, 0, cue.transform.position.z)).normalized;

			if (Input.GetButtonDown("Fire1")) {
				gameController.currentState = new GameStates.StrikingState(gameController);
			}
		}
	}
}