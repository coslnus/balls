﻿using UnityEngine;

namespace GameStates {
	public class WaitingForNextTurnState : AbstractGameObjectState {
		private GameController gameController;

		private GameObject whiteBall;
		private GameObject balls;

		public WaitingForNextTurnState(MonoBehaviour parent) : base(parent) {
			gameController = (GameController)parent;

			whiteBall = gameController.whiteBall;
			balls = gameController.balls;
		}

		public override void FixedUpdate() {
			Rigidbody whiteBallRigidbody = whiteBall.GetComponent<Rigidbody>();
			if (!(whiteBallRigidbody.IsSleeping () || whiteBallRigidbody.velocity == Vector3.zero)) {
				return;
			}

			foreach (Rigidbody rigidbody in balls.GetComponentsInChildren<Rigidbody>()) {
				if (!(rigidbody.IsSleeping () || rigidbody.velocity == Vector3.zero)) {
					return;
				}
			}

			if (gameController.tempPocketBallCount > 0 && !gameController.whiteBallIsPocketed) {
				gameController.BallsPocketed();
			}

			gameController.NextPlayer();
			gameController.currentState = new WaitingForStrikeState(gameController);
		}
	}
}