﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	Text score;

	void Start () {
		score = GetComponent<Text>();
	}

	void Update () {
		Player currentPlayer = GameController.GameControllerInstance.currentPlayer;
		Player otherPlayer = GameController.GameControllerInstance.otherPlayer;

		score.text = string.Format (
			"{0}: {1} \t\t {2}: {3}", 
			currentPlayer.Name, currentPlayer.Points, 
			otherPlayer.Name, otherPlayer.Points
		);
	}
}
