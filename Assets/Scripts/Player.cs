﻿using UnityEngine;

public class Player {
	private int ballsCollected = 0;

	public Player(string name) {
		Name = name;
	}

	public string Name {
		get;
		private set;
	}

	public int Points {
		get { return ballsCollected; }
	}

	public void CollectBall(int count) {
		ballsCollected += count;
	}
}