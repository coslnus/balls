﻿using UnityEngine;

public class BallController : MonoBehaviour {

	Rigidbody rigidbody;

	void Start() {
		rigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
		if (rigidbody.velocity.y > 0) {
			var velocity = rigidbody.velocity;
			velocity.y *= 0.3f;
			rigidbody.velocity = velocity;
		}
	}
}