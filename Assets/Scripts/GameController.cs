﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public GameObject cue;
	public GameObject cuePivot;
	public GameObject whiteBall;
	public GameObject balls;

	internal float maxForce = 4000f;
	internal float minForce = 10f;
	internal float minDistance = 17f;
	internal float maxDistance = 21f;
	internal Vector3 strikeDirection = Vector3.zero;

	public AbstractGameObjectState currentState;
	public Transform cloth;
	public Player currentPlayer;
	public Player otherPlayer;
	public Text score;
	public GameObject GameOverMessage;

	public int commonBallsAmount;
	public int tempPocketBallCount = 0;
	public bool whiteBallIsPocketed = false;
	public bool currentPlayerContinuesToPlay;

	static public GameController GameControllerInstance {
		get;
		private set;
	}

	void Start () {
		GameControllerInstance = this;
		currentState = new GameStates.WaitingForStrikeState (this);

		setPyramid ();
		setWhiteBall ();

		currentPlayer = new Player("PlayerOne");
		otherPlayer = new Player("PlayerTwo");
		commonBallsAmount = balls.transform.childCount;
	}
	
	void Update () {
		currentState.Update();
	}

	void FixedUpdate () {
		currentState.FixedUpdate();
	}

	private void setWhiteBall() {
		whiteBall.transform.position = new Vector3 (cloth.lossyScale.x / 4, whiteBall.transform.lossyScale.x / 2, 0);
	}

	private void setPyramid() {
		if (balls.transform.childCount > 0) {
			float ballDiameter = balls.transform.GetChild(0).lossyScale.x;
			float ballRadius = ballDiameter / 2;
			Vector3 pyramidStartPosition = new Vector3 (-cloth.lossyScale.x / 4, ballRadius, 0);
			Vector3 firstBallPosition = pyramidStartPosition;
			firstBallPosition.x = pyramidStartPosition.x - 0.3f;
			balls.transform.GetChild(0).position = firstBallPosition;
			float startLineZ;

			for (int i = 1, max = balls.transform.childCount; i < max; i++) {
				if (i < 3) {
					startLineZ = -ballRadius;
					balls.transform.GetChild(i).position = new Vector3 (
						pyramidStartPosition.x - ballDiameter - 0.15f, 
						ballRadius, 
						startLineZ + ballDiameter * (i - 1)
					);
				} else if (i < 6) {
					startLineZ = -ballDiameter;
					balls.transform.GetChild(i).position = new Vector3 (
						pyramidStartPosition.x - ballDiameter * 2, 
						ballRadius, 
						startLineZ + ballDiameter * (i - 3)
					);
				}
			}
		}
	}

	public void WhiteBallPocketed() {
		whiteBallIsPocketed = true;
	}

	public void BallsPocketed() {
		currentPlayerContinuesToPlay = true;
		currentPlayer.CollectBall(tempPocketBallCount);
		ClearTempBallPocketed ();
	}

	public void AddTempBallPocketed() {
		tempPocketBallCount++;
	}

	private void ClearTempBallPocketed() {
		tempPocketBallCount = 0;
	}

	public void NextPlayer() {
		if (currentPlayerContinuesToPlay && !whiteBallIsPocketed) {
			currentPlayerContinuesToPlay = false;
			return;
		}

		whiteBallIsPocketed = false;
		commonBallsAmount -= tempPocketBallCount;
		ClearTempBallPocketed ();
		Player temp = currentPlayer;
		currentPlayer = otherPlayer;
		otherPlayer = temp;
	}

	public void EndMatch() {
		Player winner = null;
		if (currentPlayer.Points > otherPlayer.Points) {
			winner = currentPlayer; 
		} else if (currentPlayer.Points < otherPlayer.Points) {
			winner = otherPlayer;
		}

		var msg = "Game Over\n";

		if (winner != null) {
			msg += string.Format (winner.Name + " wins");
			GameOverMessage.GetComponent<Text>().text = msg;
			GameOverMessage.SetActive(true);
		}
	}
}
